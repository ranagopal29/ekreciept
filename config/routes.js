/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/recipt': { controller: "EkReciptController", action:"reciptList", view: 'session/recipt-list' },
  '/recipt/:id': { controller: "EkReciptController", action:"findOne", view: 'session/default-recipt' },
  '/recipt/create': { view: 'session/recipt-create' },

  "POST /api/v1/recipt" : 'EkReciptController.create',
  "GET /api/v1/recipt" : 'EkReciptController.find',
  "GET /api/v1/recipt/:id" : 'EkReciptController.findOne',
  "DELETE /api/v1/recipt/:id" : 'EkReciptController.destroyOne',
  "PATCH /api/v1/recipt/:id": 'EkReciptController.findOne',

  "POST /api/v1/user" : 'UserController.create',
  "GET /api/v1/user" : 'UserController.getUser',
  // "GET /api/v1/user/:id" : 'UserController.findOne',
  // "PATCH /api/v1/user/:id" : 'UserController.findOne',
  // "DELETE /api/v1/user/:id" : 'UserController.findOne',
  "POST /api/v1/user/login" : 'UserController.login',

  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


};
