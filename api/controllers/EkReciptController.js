/**
 * EkReciptController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */


module.exports = {
    reciptList: async (req, res) => {
        console.log
        const reciptRecords = await EkRecipt.find()
        let recipts = JSON.stringify(reciptRecords);
        res.render("session/recipt-list", {
            recipts: recipts
        })

    },
    create: async (req, res, next) => {
        EkRecipt.create(req.body, function userCreated(err, recipt) {
            if (err) {
                res.redirect('/login/login')

                return next(err)
            };

           return res.json(recipt);
        })
        // try {
        //     let recipt = {}
        //     recipt.reciptNo = req.body.reciptNo;
        //     recipt.entity = req.body.entity.trim();
        //     recipt.amount = +req.body.amount;
        //     console.log(1)
        //     const ekrecipt = await EkRecipt.create(req.body).fetch();
        //     console.log(1)
        //     return res.status(201).json({
        //         status: 'success',
        //         data: ekrecipt
        //     });
        // }
        // catch (err) {
        //     console.log(err);
        //     return 'invalid';
        // }

    },
    find: async (req, res) => {
        const reciptRecords = await EkRecipt.find()
        if (!reciptRecords) {
            sails.log('No record Found');
          }
        return res.status(200).json({
            status: 'success',
            totalRecords: reciptRecords.length,
            data: reciptRecords
        })
    },
    findOne: async (req, res) => {
        const id = req.params.id;
        const reciptRecord = await EkRecipt.findOne({ id: id })
        if (!reciptRecord) {
            sails.log('No record Found');
        }
        // sending data in ejs
        let recipt = JSON.stringify(reciptRecord);
        res.render("session/default-recipt", {
            rcpt: reciptRecord
        });
        // data sent
        return res.status(200).json({
            status: 'success',
            data: reciptRecord
        })
    },
    updateOne: async (req, res) => {
        const id = req.params.id;
        var updatedUser = await User.updateOne({ id: id })
        .set(req.body);

        if (updatedUser) {
          sails.log('Record updated successfully');
        }
        else {
          sails.log('No record found with given enquiry');
        }
    },
    destroyOne: async (req, res) => {
        const id = req.params.id;
        var destroyedRecords = await EkRecipt.destroy({ id: id }).fetch()
        if (!destroyedRecords) {
            sails.log('No record Found');
          }
        return res.status(200).json({
            status: 'success',
            data: {
                message: "Deleted Succesfully!"
            }
        })
    }

};

