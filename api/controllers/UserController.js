/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
    "loginPage": function (req, res) {
        res.view("/login/login");
        
    },

    create: async function (req, res, next) {
        try {
            var createdRecord = await User.create(req.body).fetch();
            sails.log('Finn\'s id is');
            return res.json({
                status: "success",
                
            });
        } catch (err) {
            return err
        }
       
        // User.create(req.params.all(), function userCreated(err, user) {
        //     if (err) {
        //         req.session.flash = {
        //             err: err
        //         }
        //         req.locals.flash = _.clone(req.session.flesh)
        //         return res.redirect('/login/login')
        //     };

        //    return res.json(user);
        // })
        
    },

    getUser: function (req, res, next) {
        User.findOne(req.param('id', function foundUser(err, user) {
            if (err) return next(err);
            if (!user) return next("User doen't exist!");
            res.view({
                user: user
            })
        })
        )
    }




};

